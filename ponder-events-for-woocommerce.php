<?php
/**
 * Plugin Name:          Ponder Events for WooCommerce
 * Plugin URI:           https://www.pondermatic.com/ponder-events-for-woocommerce/
 * Description:          Sell tickets to events, conferences, and seminars with WooCommerce.
 * Version:              1.0.0
 * Requires at least:    5.8
 * Requires PHP:         7.4
 * WC requires at least: 3.5.0
 * WC tested up to:      8.6.1
 * Author:               Pondermatic
 * Author URI:           https://www.pondermatic.com/
 * Copyright:            2024 Pondermatic
 * License:              GPLv3
 * License URI:          http://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain:          ponder-events-fwc
 * Domain Path:          /languages
 * Update URI:           https://wordpress.org/plugins/ponder-events-for-woocommerce/
 *
 * @todo add logo
 */

/**
 * This file is part of Ponder Events for WooCommerce.
 *
 * Ponder Events for WooCommerce is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Ponder Events for WooCommerce is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Ponder Events for WooCommerce. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free;

defined( 'ABSPATH' ) || exit;

// Register an autoloader for this plugin.
require_once __DIR__ . '/vendor/autoload.php';

new Core();
