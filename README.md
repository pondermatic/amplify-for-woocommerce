# Ponder Events for WooCommerce

## [PHPUnit](https://phpunit.de/)

To set up for testing, execute `composer test-install`, 
which does the following:
- creates a temporary directory
- downloads WordPress to the temporary directory
- downloads the WordPress PHPUnit tests library to the temporary directory
- creates a database that is used by the tests
- uses the following environment variables:

| Name                   | Default                                  |
|------------------------|------------------------------------------|
| `PEFW_DB_HOST`        | `localhost`                              |
| `PEFW_DB_USER`        | `root`                                   |
| `PEFW_DB_PASS`        | `password`                               |
| `PEFW_DB_NAME`        | `test_ponder-events-pro-for-woocommerce` |
| `PEFW_DB_DELETE`      | `yes`                                    |
| `PEFW_SKIP_DB_CREATE` | `no`                                     |
| `PEFW_TEST_DIR`       | `./tmp`                                  |
| `PEFW_WP_VERSION`     | `latest`                                 |
