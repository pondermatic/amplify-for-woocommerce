<?php
/**
 * Stock_Availability class
 *
 * @since   1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\PHPUnit;

use Pondermatic\PonderEventsForWooCommerce\Free as Free;
use Pondermatic\PonderEventsForWooCommerce\Free\PHPUnit\Framework\Stock_Availability_Text;
use Pondermatic\WordpressPhpunitFramework\Test_Case;

/**
 * Tests the 'woocommerce_get_availability_text' filter in WC_Product::get_availability_text() with
 * Pondermatic\PonderEventsForWooCommerce\Free\FrontEnd\Stock_Messages::filter_availability_text().
 *
 * A unit test class can not extend another unit test class or else the tests within the base case
 * are called twice.
 *
 * @see WC_Product::get_availability_text()
 * @see \Pondermatic\PonderEventsForWooCommerce\Free\FrontEnd\Stock_Messages::filter_availability_text()
 *
 * @since 1.0.0
 */
abstract class Abstract_Stock_Availability extends Test_Case {

	/**
	 * An array of WooCommerce stock availability texts.
	 *
	 * @since 1.0.0
	 * @var string[]
	 */
	protected static $texts = [];

	/**
	 * Test WooCommerce stock format is "always show quantity" and product backorders is 'no'.
	 *
	 * @since 1.0.0
	 * @return Stock_Availability_Text[]
	 */
	public function provide_always_show_qty_and_backorders_no() {

		$stock_test = ( new Stock_Availability_Text() )
			->set_option_stock_format( Stock_Availability_Text::QTY_FORMAT_ALWAYS )
			->set_product_backorders( Stock_Availability_Text::BACKORDER_NO );

		$tests   = [];
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( sprintf( static::$texts['in_stock_plural'], $stock_test::STOCK_HIGH_PLURAL ) );
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( sprintf( static::$texts['in_stock_plural'], $stock_test::STOCK_LOW_PLURAL ) );
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( static::$texts['out_of_stock'] );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( '' );

		return $tests;
	}

	/**
	 * Test WooCommerce stock format is "always show quantity" and product backorders is 'notify'.
	 *
	 * @since 1.0.0
	 * @return Stock_Availability_Text[]
	 */
	public function provide_always_show_qty_and_backorders_notify() {

		$stock_test = ( new Stock_Availability_Text() )
			->set_option_stock_format( Stock_Availability_Text::QTY_FORMAT_ALWAYS )
			->set_product_backorders( Stock_Availability_Text::BACKORDER_NOTIFY );

		$tests   = [];
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text(
				sprintf( static::$texts['in_stock_plural'], $stock_test::STOCK_HIGH_PLURAL ) .
				' ' . static::$texts['can_be_backordered']
			);
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text(
				sprintf( static::$texts['in_stock_plural'], $stock_test::STOCK_LOW_PLURAL ) .
				' ' . static::$texts['can_be_backordered']
			);
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( static::$texts['available_on_backorder'] );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( '' );

		return $tests;
	}

	/**
	 * Test WooCommerce stock format is "always show quantity" and product backorders is 'yes'.
	 *
	 * @since 1.0.0
	 * @return Stock_Availability_Text[]
	 */
	public function provide_always_show_qty_and_backorders_yes() {

		$stock_test = ( new Stock_Availability_Text() )
			->set_option_stock_format( Stock_Availability_Text::QTY_FORMAT_ALWAYS )
			->set_product_backorders( Stock_Availability_Text::BACKORDER_YES );

		$tests   = [];
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( sprintf( static::$texts['in_stock_plural'], $stock_test::STOCK_HIGH_PLURAL ) );
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( sprintf( static::$texts['in_stock_plural'], $stock_test::STOCK_LOW_PLURAL ) );
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( 'unfiltered availability text' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( '' );

		return $tests;
	}

	/**
	 * Test WooCommerce stock format is "never show quantity" and product backorders is 'no'.
	 *
	 * @since 1.0.0
	 * @return Stock_Availability_Text[]
	 */
	public function provide_never_show_qty_and_backorders_no() {

		$stock_test = ( new Stock_Availability_Text() )
			->set_option_stock_format( Stock_Availability_Text::QTY_FORMAT_NO )
			->set_product_backorders( Stock_Availability_Text::BACKORDER_NO );

		$tests   = [];
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( static::$texts['in_stock'] );
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( static::$texts['in_stock'] );
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( static::$texts['out_of_stock'] );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( '' );

		return $tests;
	}

	/**
	 * Test WooCommerce stock format is "never show quantity" and product backorders is 'notify'.
	 *
	 * @since 1.0.0
	 * @return Stock_Availability_Text[]
	 */
	public function provide_never_show_qty_and_backorders_notify() {

		$stock_test = ( new Stock_Availability_Text() )
			->set_option_stock_format( Stock_Availability_Text::QTY_FORMAT_NO )
			->set_product_backorders( Stock_Availability_Text::BACKORDER_NOTIFY );

		$tests   = [];
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text(
				static::$texts['in_stock'] . ' ' . static::$texts['can_be_backordered']
			);
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text(
				static::$texts['in_stock'] . ' ' . static::$texts['can_be_backordered']
			);
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( static::$texts['available_on_backorder'] );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( '' );

		return $tests;
	}

	/**
	 * Test WooCommerce stock format is "never show quantity" and product backorders is 'yes'.
	 *
	 * @since 1.0.0
	 * @return Stock_Availability_Text[]
	 */
	public function provide_never_show_qty_and_backorders_yes() {

		$stock_test = ( new Stock_Availability_Text() )
			->set_option_stock_format( Stock_Availability_Text::QTY_FORMAT_NO )
			->set_product_backorders( Stock_Availability_Text::BACKORDER_YES );

		$tests   = [];
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( static::$texts['in_stock'] );
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( static::$texts['in_stock'] );
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( 'unfiltered availability text' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( '' );

		return $tests;
	}

	/**
	 * Test WooCommerce stock format is "show low quantity" and product backorders is 'no'.
	 *
	 * @since 1.0.0
	 * @return Stock_Availability_Text[]
	 */
	public function provide_show_low_qty_and_backorders_no() {

		$stock_test = ( new Stock_Availability_Text() )
			->set_option_stock_format( Stock_Availability_Text::QTY_FORMAT_LOW )
			->set_product_backorders( Stock_Availability_Text::BACKORDER_NO );

		$tests   = [];
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( static::$texts['in_stock'] );
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( sprintf( static::$texts['low_stock_plural'], $stock_test::STOCK_LOW_PLURAL ) );
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( static::$texts['out_of_stock'] );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( '' );

		return $tests;
	}

	/**
	 * Test WooCommerce stock format is "show low quantity" and product backorders is 'notify'.
	 *
	 * @since 1.0.0
	 * @return Stock_Availability_Text[]
	 */
	public function provide_show_low_qty_and_backorders_notify() {

		$stock_test = ( new Stock_Availability_Text() )
			->set_option_stock_format( Stock_Availability_Text::QTY_FORMAT_LOW )
			->set_product_backorders( Stock_Availability_Text::BACKORDER_NOTIFY );

		$tests   = [];
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text(
				static::$texts['in_stock'] . ' ' . static::$texts['can_be_backordered']
			);
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text(
				sprintf( static::$texts['low_stock_plural'], $stock_test::STOCK_LOW_PLURAL ) .
				' ' . static::$texts['can_be_backordered']
			);
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( static::$texts['available_on_backorder'] );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( '' );

		return $tests;
	}

	/**
	 * Test WooCommerce stock format is "show low quantity" and product backorders is 'yes'.
	 *
	 * @since 1.0.0
	 * @return Stock_Availability_Text[]
	 */
	public function provide_show_low_qty_and_backorders_yes() {

		$stock_test = ( new Stock_Availability_Text() )
			->set_option_stock_format( Stock_Availability_Text::QTY_FORMAT_LOW )
			->set_product_backorders( Stock_Availability_Text::BACKORDER_YES );

		$tests   = [];
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( static::$texts['in_stock'] );
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( sprintf( static::$texts['low_stock_plural'], $stock_test::STOCK_LOW_PLURAL ) );
		$tests[] = clone $stock_test
			->set_product_manage_stock( true )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( 'unfiltered availability text' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_HIGH_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_LOW_PLURAL )
			->set_expected_text( '' );
		$tests[] = clone $stock_test
			->set_product_manage_stock( false )
			->set_product_stock( $stock_test::STOCK_OUT )
			->set_expected_text( '' );

		return $tests;
	}

	/**
	 * Run stock availability tests.
	 *
	 * @since 1.0.0
	 * @param Stock_Availability_Text[] $tests An array of stock availability objects.
	 */
	abstract protected function run_tests_on_stock_object( $tests );

	/**
	 * This method is called before each test.
	 *
	 * @since 1.0.0
	 */
	public function set_up() {

		parent::set_up();

		// Manage stock.
		update_option( 'woocommerce_manage_stock', 'yes' );

		// Low stock threshold.
		update_option( 'woocommerce_notify_low_stock_amount', '2' );

		// Out of stock threshold.
		update_option( 'woocommerce_notify_no_stock_amount', '0' );

		// Stock display format.
		update_option( 'woocommerce_stock_format', Stock_Availability_Text::QTY_FORMAT_ALWAYS );
	}

	/**
	 * This method is called before the first test of this test class is run.
	 *
	 * @since 1.0.0
	 */
	public static function set_up_before_class() {

		parent::set_up_before_class();

		$model = Free\Models\Stock_Messages::new_with_array( static::$texts );
		$model->set_id( 1 );
		$model->save();
	}

	/**
	 * Tests the FrontEnd\Stock_Messages::filter_availability_text() with every combination of
	 * variables that affect the availability text.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function test_filter_availability_text() {

		$this->run_tests_on_stock_object( $this->provide_always_show_qty_and_backorders_no() );
		$this->run_tests_on_stock_object( $this->provide_always_show_qty_and_backorders_notify() );
		$this->run_tests_on_stock_object( $this->provide_always_show_qty_and_backorders_yes() );
		$this->run_tests_on_stock_object( $this->provide_never_show_qty_and_backorders_no() );
		$this->run_tests_on_stock_object( $this->provide_never_show_qty_and_backorders_notify() );
		$this->run_tests_on_stock_object( $this->provide_never_show_qty_and_backorders_yes() );
		$this->run_tests_on_stock_object( $this->provide_show_low_qty_and_backorders_no() );
		$this->run_tests_on_stock_object( $this->provide_show_low_qty_and_backorders_notify() );
		$this->run_tests_on_stock_object( $this->provide_show_low_qty_and_backorders_yes() );
	}
}
