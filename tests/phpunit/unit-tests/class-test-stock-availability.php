<?php
/**
 * Test_Stock_Availability class
 *
 * @since   1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\PHPUnit;

use Pondermatic\PonderEventsForWooCommerce\Free\PHPUnit\Framework\Stock_Availability_Text;

/**
 * Tests the 'woocommerce_get_availability_text' filter in WC_Product::get_availability_text() with
 * Pondermatic\PonderEventsForWooCommerce\Free\FrontEnd\Stock_Messages::filter_availability_text().
 *
 * @see WC_Product::get_availability_text()
 * @see \Pondermatic\PonderEventsForWooCommerce\Free\FrontEnd\Stock_Messages::filter_availability_text()
 *
 * @since 1.0.0
 */
class Test_Stock_Availability extends Abstract_Stock_Availability {

	/**
	 * An array of WooCommerce stock availability texts.
	 *
	 * @since 1.0.0
	 * @var string[]
	 */
	protected static $texts = [
		'out_of_stock'           => 'custom Out of stock',
		'available_on_backorder' => 'custom Available on backorder',
		'in_stock'               => 'custom In stock',
		'low_stock_plural'       => 'custom Only %s left in stock',
		'in_stock_plural'        => 'custom %s in stock',
		'can_be_backordered'     => 'custom (can be backordered)',
	];

	/**
	 * Run stock availability tests.
	 *
	 * @since 1.0.0
	 * @param Stock_Availability_Text[] $tests An array of stock availability objects.
	 */
	protected function run_tests_on_stock_object( $tests ) {

		$current_format = null;

		foreach ( $tests as $key => $test ) {

			if ( $current_format !== $test->option_stock_format ) {
				$current_format = $test->option_stock_format;
				update_option( 'woocommerce_stock_format', $test->option_stock_format );
			}

			$test->set_product( self::factory()->product_simple->create_object() );
			$test->product->set_backorders( $test->product_backorders );
			$test->product->set_low_stock_amount( $test->product_low_stock_amount );
			$test->product->set_manage_stock( $test->product_manage_stock );
			$test->product->set_stock_quantity( $test->product_stock_quantity );
			$test->product->validate_props(); // Set's the product's `stock_status` property.

			$actual_text = apply_filters(
				'woocommerce_get_availability_text',
				'unfiltered availability text',
				$test->product
			);

			$this->assertEquals( $test->expected_text, $actual_text, $test->get_assert_message( $key ) );
		}
	}
}
