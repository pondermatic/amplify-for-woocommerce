<?php
/**
 * Loads and instantiates the PHHPUnit bootstrap class for this plugin.
 *
 * @since 1.0.0
 * @version 1.0.0
 */

use Pondermatic\PonderEventsForWooCommerce\Free\PHPUnit\Bootstrap;

require 'class-bootstrap.php';

/* @noinspection PhpExpressionResultUnusedInspection */
new Bootstrap();
