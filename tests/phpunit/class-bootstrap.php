<?php
/**
 * Bootstrap class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\PHPUnit;

use Pondermatic\WordpressPhpunitFramework\Bootstrap as FrameworkBoostrap;
use WC_Install;

/**
 * PHPUnit bootstrap.
 *
 * @since 1.0.0
 */
class Bootstrap extends FrameworkBoostrap {

	/**
	 * Main PHP File for the plugin.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $plugin_main = 'ponder-events-for-woocommerce.php';

	/**
	 * Install the plugin being tested and any plugins it is integrated with.
	 *
	 * @since 1.0.0
	 */
	public function install() {

		WC_Install::install();
	}

	/**
	 * Manually load the plugin being tested and any dependencies.
	 *
	 * @since 1.0.0
	 */
	public function load() {

		parent::load();
		$this->load_plugin( 'woocommerce', 'woocommerce.php' );
	}
}
