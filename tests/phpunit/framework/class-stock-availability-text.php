<?php
/**
 * Stock_Availability_Text class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\PHPUnit\Framework;

use WC_Product;

/**
 * A class to help test the many variables that affect the stock availability text.
 *
 * After setting the product's stock properties and before filtering the availability text,
 * {@see WC_Product::validate_props()} should be called to set the product's stock status.
 *
 * @since 1.0.0
 */
class Stock_Availability_Text {

	/**
	 * Product can not be backordered.
	 *
	 * @since 1.0.0
	 */
	public const BACKORDER_NO = 'no';

	/**
	 * Product can be backordered, but notify the customer.
	 *
	 * @since 1.0.0
	 */
	public const BACKORDER_NOTIFY = 'notify';

	/**
	 * Product can be backordered.
	 *
	 * @since 1.0.0
	 */
	public const BACKORDER_YES = 'yes';

	/**
	 * Always show quantity remaining in stock e.g. "12 in stock".
	 *
	 * @since 1.0.0
	 */
	public const QTY_FORMAT_ALWAYS = '';

	/**
	 * Only show quantity remaining in stock when low e.g. "Only 2 left in stock".
	 *
	 * @since 1.0.0
	 */
	public const QTY_FORMAT_LOW = 'low_amount';

	/**
	 * Never show quantity remaining in stock.
	 *
	 * @since 1.0.0
	 */
	public const QTY_FORMAT_NO = 'no_amount';

	/**
	 * Stock quantity is above the low stock amount threshold.
	 *
	 * @since 1.0.0
	 */
	public const STOCK_HIGH_PLURAL = 9;

	/**
	 * Stock quantity is 1 and above the low stock amount threshold.
	 *
	 * @since 1.0.0
	 */
	public const STOCK_HIGH_SINGULAR = '1_high';

	/**
	 * Stock quantity is less than or equal to the low stock amount threshold.
	 *
	 * @since 1.0.0
	 */
	public const STOCK_LOW_PLURAL = 2;

	/**
	 * Stock quantity is less than or equal to the low stock amount threshold.
	 *
	 * @since 1.0.0
	 */
	public const STOCK_LOW_SINGULAR = '1_low';

	/**
	 * Stock quantity is 0.
	 *
	 * @since 1.0.0
	 */
	public const STOCK_OUT = 0;

	/**
	 * The expected availability text returned when testing a method.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	public string $expected_text = '';

	/**
	 * Stock format to set in the `woocommerce_stock_format` WordPress option.
	 *
	 * @since 1.0.0
	 * @var string ''|'low_amount'|'no_amount'
	 */
	public string $option_stock_format = self::QTY_FORMAT_ALWAYS;

	/**
	 * A product object.
	 *
	 * @since 1.0.0
	 * @var WC_Product
	 */
	public WC_Product $product;

	/**
	 * Product `backorders` property.
	 *
	 * @since 1.0.0
	 * @var string yes|no|notify
	 */
	public string $product_backorders = self::BACKORDER_YES;

	/**
	 * Product `low_stock_amount` property.
	 *
	 * For testing, always set to an `int`.
	 *
	 * @since 1.0.0
	 * @var int|string An empty '' string means it is not set.
	 */
	public int $product_low_stock_amount;

	/**
	 * Product `manage_stock` property.
	 *
	 * @since 1.0.0
	 * @var bool
	 */
	public bool $product_manage_stock = true;

	/**
	 * Product `stock_quantity` property.
	 *
	 * For testing, always set to an `int`.
	 *
	 * @since 1.0.0
	 * @var int|null
	 */
	public int $product_stock_quantity;

	/**
	 * The text before being filtered.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	public string $unfiltered_text = '';

	/**
	 * Returns a message about this test's properties for the PHPUnit assert methods.
	 *
	 * @since 1.0.0
	 * @param int|string $key The optional key of the test in an array of tests.
	 * @return string
	 */
	public function get_assert_message( $key = null ) {

		switch ( $this->option_stock_format ) {
			case self::QTY_FORMAT_ALWAYS:
				$format = 'Always show quantity remaining in stock e.g. "12 in stock"';
				break;
			case self::QTY_FORMAT_LOW:
				$format = 'Only show quantity remaining in stock when low e.g. "Only 2 left in stock"';
				break;
			case self::QTY_FORMAT_NO:
				$format = 'Never show quantity remaining in stock';
				break;
			default:
				$format = 'Unknown stock display format!';
		}
		$vars = [
			'option_stock_format'      => $format,
			'product_backorders'       => "'$this->product_backorders'",
			'product_low_stock_amount' => $this->product_low_stock_amount,
			'product_manage_stock'     => $this->product_manage_stock ? 'true' : 'false',
			'product_stock_quantity'   => $this->product_stock_quantity,
			'product_stock_status'     => "'{$this->product->get_stock_status()}'",
		];

		/* @noinspection SpellCheckingInspection */
		$max_key_len = max( array_map( 'strlen', array_keys( $vars ) ) );
		$message     = is_null( $key ) ? '' : "Test key $key:\n";

		foreach ( $vars as $name => $value ) {
			$message .= str_pad( $name, $max_key_len, ' ' ) . ": $value\n";
		}

		return $message;
	}

	/**
	 * Sets the `expected_text` property.
	 *
	 * @since 1.0.0
	 * @param string $value The new property value.
	 * @return $this
	 */
	public function set_expected_text( string $value ): self {

		$this->expected_text = $value;
		return $this;
	}

	/**
	 * Sets the `woocommerce_stock_format` option.
	 *
	 * @since 1.0.0
	 * @param string $value The new property value.
	 * @return $this
	 */
	public function set_option_stock_format( string $value ): self {

		$this->option_stock_format = $value;
		return $this;
	}

	/**
	 * Sets the `product` property.
	 *
	 * @since 1.0.0
	 * @param WC_Product $product The new property value.
	 * @return $this
	 */
	public function set_product( WC_Product $product ): self {

		$this->product = $product;
		return $this;
	}

	/**
	 * Sets the product's `backorders` property.
	 *
	 * @since 1.0.0
	 * @param string $value Options: 'yes', 'no' or 'notify'.
	 * @return $this
	 */
	public function set_product_backorders( string $value ): self {

		$this->product_backorders = $value;
		return $this;
	}

	/**
	 * Sets the product's `low_stock_amount` property.
	 *
	 * @since 1.0.0
	 * @param int $value The new property value.
	 * @return $this
	 */
	public function set_product_low_stock_amount( int $value ): self {

		$this->product_low_stock_amount = $value;
		return $this;
	}

	/**
	 * Sets the product's `manage_stock` property.
	 *
	 * @since 1.0.0
	 * @param bool $value The new property value.
	 * @return $this
	 */
	public function set_product_manage_stock( bool $value ): self {

		$this->product_manage_stock = wc_string_to_bool( $value );
		return $this;
	}

	/**
	 * Sets the product's `low_stock_amount` and `stock_quantity` properties.
	 *
	 * @since 1.0.0
	 * @param int|string $value {@see self::STOCK_HIGH_PLURAL}|{@see self::STOCK_HIGH_SINGULAR}|
	 *                          {@see self::STOCK_LOW_PLURAL}|{@see self::STOCK_LOW_SINGULAR}|
	 *                          {@see self::STOCK_OUT}.
	 * @return $this
	 */
	public function set_product_stock( $value ): self {

		switch ( $value ) {
			case self::STOCK_HIGH_PLURAL:
				$this->product_low_stock_amount = 6;
				$this->product_stock_quantity   = 9;
				break;
			case self::STOCK_HIGH_SINGULAR:
				$this->product_low_stock_amount = 0;
				$this->product_stock_quantity   = 1;
				break;
			case self::STOCK_LOW_PLURAL:
				$this->product_low_stock_amount = 4;
				$this->product_stock_quantity   = 2;
				break;
			case self::STOCK_LOW_SINGULAR:
				$this->product_low_stock_amount = 3;
				$this->product_stock_quantity   = 1;
				break;
			case self::STOCK_OUT:
				$this->product_low_stock_amount = 5;
				$this->product_stock_quantity   = 0;
				break;
		}
		return $this;
	}

	/**
	 * Sets the product's `stock_quantity` property.
	 *
	 * @since 1.0.0
	 * @param int $value The new property value.
	 * @return $this
	 */
	public function set_product_stock_quantity( int $value ): self {

		$this->product_stock_quantity = $value;
		return $this;
	}

	/**
	 * Set's the `unfiltered_text` property.
	 *
	 * @since 1.0.0
	 * @param string $value The text before being filtered.
	 * @return $this
	 */
	public function set_unfiltered_text( string $value ): self {

		$this->unfiltered_text = "unfiltered $value";
		return $this;
	}
}
