=== Ponder Events for WooCommerce ===
Contributors: pondermatic
Donate link: https://www.pondermatic.com/
Tags: woocommerce, stock messages, SKU, events, tickets
Requires at least: 5.8
Tested up to: 6.4.3
Stable tag: 1.0.0
Requires PHP: 7.4
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Sell tickets to conferences and events with WooCommerce.

== Description ==

[Ponder Events for WooCommerce](https://www.pondermatic.com/ponder-events-for-woocommerce/) is the perfect plugin to help sell tickets to conferences. It allows you to change the WooCommerce stock messages. For example, the low stock quantity message can be changed from "Only 5 left in stock" message, to "Only 5 seats left!".

## Features at a glance

* Customize stock messages
* Disable the "Add to cart" button after a closing date
* Display a message before and after the closing date
* Disable display of "SKU: N/A" if all variations of a variable product do not have a SKU (Stock Keeping Unit)

## Features in more detail

**Stock Messages** - These are the stock messages that can be changed.

* Available on backorder
* Can be back ordered
* Out of stock
* When the stock display format is set to **Always show quantity remaining in stock e.g. "12 in stock"**:
  * In stock with quantity > 1
  * In stock with quantity = 1  _(available in premium version)_
* When the stock display format is set to **Never show quantity remaining in stock**:
  * In stock
* When the stock display format is set to **Only show quantity remaining in stock when low e.g. "Only 2 left in stock"**:
  * Low stock quantity > 1
  * Low stock quantity = 1 _(available in premium version)_

**Closing Date** - By setting a "Closing date", the "Add to cart" button can be disabled after the date and a message will be displayed before and after the closing date.

* Today <= product closing date
* Today > product closing date

**SKU** - The product SKU (Stock Keeping Unit) display can be disabled if all variations of a variable product do not have one. WooCommerce usually displays "SKU: N/A" on the product page if a variation of a variable product does not have an SKU and at least one other variation does.

**Product Types** - Works with both simple and variable products.

## Pro version additional features

**Plural and Singular Quantity** - Allows the quantity stock message to be customized for plural and singular quantities. For example, "_%s seats left_" and "_%s seat left_".

* When the stock display format is set to **Always show quantity remaining in stock e.g. "12 in stock"**:
  * In stock with quantity = 1
* When the stock display format is set to **Only show quantity remaining in stock when low e.g. "Only 2 left in stock"**:
  * Low stock quantity = 1

**Sets** - Create more than one set of stock messages. Use one set of stock messages for one kind of product, for example, events that you sell seats to, and another set of stock messages for physical products.

  * Name each set
  * Display a list of all sets
  * Delete sets individually or in bulk

**Attendee names** - Collect the names of the people attending an event during the purchase.

== Frequently Asked Questions ==

= Where is the documentation?

All documentation can be found in our [knowledge base](https://www.pondermatic.com/knowledge-base/).

= Where can I report an issue?

The best place to report an issue is on our [issue tracker](https://gitlab.com/pondermatic-llc/wordpress/ponder-events-for-woocommerce/-/issues).

You are also welcome to fork the repository, make changes, and submit a merge request.

= Feature requests

If you want a new feature, please make a request on our [issue tracker](https://gitlab.com/pondermatic-llc/wordpress/ponder-events-for-woocommerce/-/issues).

You are also welcome to fork the repository, make changes, and submit a merge request.

== Screenshots ==

1. Stock messages settings page.
2. Products settings page.
3. Closing date on the edit product page.
4. Closing date on the product page.

== Changelog ==

= 1.0.0 – 2024-03-01 =
* First public release.
