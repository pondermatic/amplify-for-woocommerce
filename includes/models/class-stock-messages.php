<?php
/**
 * Model Stock_Messages class
 *
 * @since 1.0.0
 * @version 1.0.0
 * @noinspection PhpUnused
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\Models;

use Pondermatic\PonderEventsForWooCommerce\Free\Core;
use Pondermatic\PonderEventsForWooCommerce\Free\FrontEnd as FrontEnd;
use WC_Product;

/**
 * A collection of stock messages settings.
 *
 * @since 1.0.0
 */
class Stock_Messages {

	/**
	 * Available on backorder.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $available_on_backorder;

	/**
	 * Can be backordered.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $can_be_backordered;

	/**
	 * After closing date.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $closing_date_after;

	/**
	 * Before closing date.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $closing_date_before;

	/**
	 * Enabled.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $enabled = 'yes';

	/**
	 * ID of this set of stock messages.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $id;

	/**
	 * In stock.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $in_stock;

	/**
	 * In stock plural.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $in_stock_plural;

	/**
	 * In stock singular.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $in_stock_singular;

	/**
	 * Low stock plural.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $low_stock_plural;

	/**
	 * Low stock singular.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $low_stock_singular;

	/**
	 * Name.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $name;

	/**
	 * Out of stock.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $out_of_stock;

	/**
	 * Returns the given product's stock messages.
	 *
	 * @since 1.0.0
	 * @param WC_Product $product     Product object.
	 * @param int        $settings_id The ID of the stock messages set.
	 * @return string[]|false
	 */
	public static function fetch_with_product( $product, $settings_id = 1 ) {

		$options = get_option( FrontEnd\Stock_Messages::OPTION_PREFIX . $settings_id, [] );
		if ( ! isset( $options['enabled'] ) || $options['enabled'] !== 'yes' ) {
			return false;
		}

		return $options;
	}

	/**
	 * Returns the "available on backorder" message.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_available_on_backorder() {

		return $this->available_on_backorder;
	}

	/**
	 * Returns the "can be backordered" message.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_can_be_backordered() {

		return $this->can_be_backordered;
	}

	/**
	 * Returns the "after closing date" message.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_closing_date_after() {

		return $this->closing_date_after;
	}

	/**
	 * Returns the "before closing date" message.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_closing_date_before() {

		return $this->closing_date_before;
	}

	/**
	 * Returns the enabled status of the custom stock messages.
	 *
	 * @since 1.0.0
	 * @return string 'yes'|'no
	 */
	public function get_enabled() {

		return $this->enabled;
	}

	/**
	 * Returns the ID of the stock messages set.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_id() {

		return $this->id;
	}

	/**
	 * Returns the "in stock" message.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_in_stock() {

		return $this->in_stock;
	}

	/**
	 * Returns the "in stock plural" message.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_in_stock_plural() {

		return $this->in_stock_plural;
	}

	/**
	 * Returns the "in stock singular" message.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_in_stock_singular() {

		return $this->in_stock_singular;
	}

	/**
	 * Returns the "low stock plural" message.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_low_stock_plural() {

		return $this->low_stock_plural;
	}

	/**
	 * Returns the "low stock singular" message.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_low_stock_singular() {

		return $this->low_stock_singular;
	}

	/**
	 * Returns the name of the stock messages set.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_name() {

		return $this->name;
	}

	/**
	 * Returns the "out of stock" message.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_out_of_stock() {

		return $this->out_of_stock;
	}

	/**
	 * Returns a new Stock_Messages object from the given array of properties.
	 *
	 * @since 1.0.0
	 * @param array $array Properties.
	 * @return self
	 */
	public static function new_with_array( $array ) {

		$set = new self();

		foreach ( $array as $key => $value ) {
			if ( ! property_exists( __CLASS__, $key ) ) {
				continue;
			}
			$set->{"set_$key"}( $value );
		}

		return $set;
	}

	/**
	 * Returns a Stock_Messages object with the given ID or a new Stock_Messages object.
	 *
	 * @since 1.0.0
	 * @param int $id ID of the Stock_Messages object.
	 * @return self
	 */
	public static function new_with_id( $id ) {

		$settings = get_option( FrontEnd\Stock_Messages::OPTION_PREFIX . $id );
		if ( $settings === false ) {
			return new self();
		}

		/* @var Stock_Messages $stock_messages_model */
		$stock_messages_model = Core::get_namespace() . '\Models\Stock_Messages';
		$settings             = $stock_messages_model::new_with_array( $settings );
		$settings->set_id( $id );
		return $settings;
	}

	/**
	 * Saves this set of stock messages to the options database table.
	 *
	 * @since 1.0.0
	 * @return bool
	 */
	public function save() {

		$properties = get_object_vars( $this );
		unset( $properties['id'] );
		return update_option( FrontEnd\Stock_Messages::OPTION_PREFIX . $this->id, $properties );
	}

	/**
	 * Sets the "available on backorder" message.
	 *
	 * @since 1.0.0
	 * @param string $available_on_backorder Available on backorder message.
	 * @return $this
	 */
	public function set_available_on_backorder( $available_on_backorder ) {

		$this->available_on_backorder = $available_on_backorder;
		return $this;
	}

	/**
	 * Sets the "can be backordered" message.
	 *
	 * @since 1.0.0
	 * @param string $can_be_backordered Can be backordered message.
	 * @return $this
	 */
	public function set_can_be_backordered( $can_be_backordered ) {

		$this->can_be_backordered = $can_be_backordered;
		return $this;
	}

	/**
	 * Sets the "after closing date" message.
	 *
	 * @since 1.0.0
	 * @param string $closing_date_after After closing date message.
	 * @return $this
	 */
	public function set_closing_date_after( $closing_date_after ) {

		$this->closing_date_after = $closing_date_after;
		return $this;
	}

	/**
	 * Sets the "before closing date" message.
	 *
	 * @since 1.0.0
	 * @param string $closing_date_before Before closing date message.
	 * @return $this
	 */
	public function set_closing_date_before( $closing_date_before ) {

		$this->closing_date_before = $closing_date_before;
		return $this;
	}

	/**
	 * Sets the enabled status for the stock messages set.
	 *
	 * @since 1.0.0
	 * @param string $enabled Enabled status, 'yes'|'no'.
	 * @return $this
	 */
	public function set_enabled( $enabled ) {

		$this->enabled = $enabled;
		return $this;
	}

	/**
	 * Set the ID of this stock messages set.
	 *
	 * @since 1.0.0
	 * @param string $id Stock messages set ID.
	 * @return $this
	 */
	public function set_id( $id ) {

		$this->id = (string) $id;
		return $this;
	}

	/**
	 * Sets the "in stock" message.
	 *
	 * @since 1.0.0
	 * @param string $in_stock In stock message.
	 * @return $this
	 */
	public function set_in_stock( $in_stock ) {

		$this->in_stock = $in_stock;
		return $this;
	}

	/**
	 * Sets the "in stock plural" message.
	 *
	 * @since 1.0.0
	 * @param string $in_stock_plural In stock plural message.
	 * @return $this
	 */
	public function set_in_stock_plural( $in_stock_plural ) {

		$this->in_stock_plural = $in_stock_plural;
		return $this;
	}

	/**
	 * Sets the "in stock singular" message.
	 *
	 * @since 1.0.0
	 * @param string $in_stock_singular In stock singular message.
	 * @return $this
	 */
	public function set_in_stock_singular( $in_stock_singular ) {

		$this->in_stock_singular = $in_stock_singular;
		return $this;
	}

	/**
	 * Sets the "low stock plural" message.
	 *
	 * @since 1.0.0
	 * @param string $low_stock_plural Low stock plural message.
	 * @return $this
	 */
	public function set_low_stock_plural( $low_stock_plural ) {

		$this->low_stock_plural = $low_stock_plural;
		return $this;
	}

	/**
	 * Sets the "low stock singular" message.
	 *
	 * @since 1.0.0
	 * @param string $low_stock_singular Low stock singular message.
	 * @return $this
	 */
	public function set_low_stock_singular( $low_stock_singular ) {

		$this->low_stock_singular = $low_stock_singular;
		return $this;
	}

	/**
	 * Sets the name of the stock messages set.
	 *
	 * @since 1.0.0
	 * @param string $name Stock messages set name.
	 * @return $this
	 */
	public function set_name( $name ) {

		$this->name = $name;
		return $this;
	}

	/**
	 * Sets the "out of stock" message.
	 *
	 * @since 1.0.0
	 * @param string $out_of_stock Out of stock message.
	 * @return $this
	 */
	public function set_out_of_stock( $out_of_stock ) {

		$this->out_of_stock = $out_of_stock;
		return $this;
	}
}
