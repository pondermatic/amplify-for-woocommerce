<?php
/**
 * Hooks trait
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\Traits;

/**
 * Registers hooks.
 *
 * @since 1.0.0
 */
trait Hooks {

	/**
	 * Add hook callbacks for use on admin pages.
	 *
	 * @since 1.0.0
	 */
	public function add_admin_hooks() {
	}

	/**
	 * Add hook callbacks for use on admin or public-facing pages.
	 *
	 * @since 1.0.0
	 */
	public function add_hooks() {
	}

	/**
	 * Add hook callbacks for use on public-facing pages.
	 *
	 * @since 1.0.0
	 */
	public function add_public_hooks() {
	}
}
