<?php
/**
 * Autoloader class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free;

use Exception;

/**
 * Autoloader for the plugin.
 *
 * The composer autoloader is used instead of this is autoloader.
 *
 * @since 1.0.0
 */
class Autoloader {

	// phpcs:disable
//	/**
//	 * An array of WooCommerce files that are required to load this plugin's classes.
//	 *
//	 * @since 1.0.0
//	 * @var string[] $woocommerce_requires [ $namespaced_class => $file_relative_to_wc_abspath ]
//	 */
//	protected $woocommerce_requires = [];
//
//	/**
//	 * An array of WordPress files that are required to load this plugin's classes.
//	 *
//	 * @since 1.0.0
//	 * @var string[] $wordpress_requires [ $namespaced_class => $file_relative_to_abspath ]
//	 */
//	protected $wordpress_requires = [];
	// phpcs:enable

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		// phpcs:disable
//		$this->woocommerce_requires[ __NAMESPACE__ . '\BackEnd\Settings\Stock_Messages' ] =
//			[
//				'includes/admin/settings/class-wc-settings-page.php',
//			];
//
//		$this->woocommerce_requires[ __NAMESPACE__ . '\BackEnd\Settings\Ponder_Events' ] =
//			[
//				'includes/admin/settings/class-wc-settings-page.php',
//			];
		// phpcs:enable

		try {
			spl_autoload_register( [ $this, 'autoload' ] );
		} catch ( Exception $exception ) {
			return;
		}
	}

	/**
	 * Loads files of known classes.
	 *
	 * @since 1.0.0
	 * @param string $class Class name.
	 */
	public function autoload( $class ) {

		if ( stripos( $class, $this->get_namespace() ) !== 0 ) {
			return;
		}

		// Break into vendor/plugin/(sub-directories/)class.
		$directories = explode( '\\', $class );

		// Remove vendor name, plugin name, and version.
		$directories = array_slice( $directories, 3 );

		// Convert names to WordPress file names.
		foreach ( $directories as &$directory ) {
			$directory = str_replace( '_', '-', strtolower( $directory ) );
		}
		$file_name = array_pop( $directories ) . '.php'; // Extract class name.

		// Make partial path from subdirectories.
		$path = $directories
			? implode( DIRECTORY_SEPARATOR, $directories ) . DIRECTORY_SEPARATOR
			: '';
		$path = str_replace( [ 'backend', 'frontend' ], [ 'back-end', 'front-end' ], $path );

		// Prefix class file name.
		if ( isset( $directories[0] ) && $directories[0] === 'traits' ) {
			$file_name = 'trait-' . $file_name;
		} else {
			$file_name = 'class-' . $file_name;
		}

		$abs_path = $this->get_dir() . DIRECTORY_SEPARATOR . $path . $file_name;
		if ( ! is_readable( $abs_path ) ) {
			return;
		}

		// phpcs:disable
//		$this->include_dependencies( $class );
		// phpcs:enable

		include_once $abs_path;
	}

	/**
	 * Returns the absolute path that THIS file is in.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_dir() {

		return __DIR__;
	}

	/**
	 * Returns the namespace that THIS file is in.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_namespace() {

		return __NAMESPACE__;
	}

	// phpcs:disable
//	/**
//	 * Loads WooCommerce and WordPress files required by this plugin's classes.
//	 *
//	 * @since 1.0.0
//	 * @param string $class Class name.
//	 */
//	protected function include_dependencies( $class ) {
//
//		if (
//			defined( 'WC_ABSPATH' ) && array_key_exists( $class, $this->woocommerce_requires )
//		) {
//			foreach ( $this->woocommerce_requires[ $class ] as $require ) {
//				/** WC_ABSPATH defined in {@see WooCommerce::define_constants()} */
//				/** @noinspection PhpUndefinedConstantInspection */
//				include_once WC_ABSPATH . $require;
//			}
//		} elseif ( array_key_exists( $class, $this->wordpress_requires ) ) {
//
//			foreach ( $this->wordpress_requires[ $class ] as $require ) {
//				require_once ABSPATH . $require;
//			}
//		}
//	}
	// phpcs:enable
}
