<?php
/**
 * Back end product Closing_Date class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\BackEnd\Product;

use Pondermatic\PonderEventsForWooCommerce\Free\FrontEnd as FrontEnd;
use Pondermatic\PonderEventsForWooCommerce\Free\Traits\Hooks;
use WC_Product;
use WP_Post;

/**
 * Manages a product's closing date.
 *
 * @since 1.0.0
 */
class Closing_Date {

	use Hooks;

	/**
	 * Add hook callbacks for use on admin pages.
	 *
	 * @since 1.0.0
	 */
	public function add_admin_hooks() {

		// Display product edit page.
		add_action(
			'woocommerce_product_options_general_product_data',
			[ $this, 'output_general_options' ],
			100,
			0
		);
		add_action(
			'woocommerce_product_after_variable_attributes',
			[ $this, 'output_variable_attributes' ],
			100,
			3
		);

		// Save product "Closing Date" date.
		/* @see \WC_Meta_Box_Product_Data::save() */
		add_action(
			'woocommerce_process_product_meta_simple',
			[ $this, 'update_simple_product' ],
			100,
			2
		);
		add_action(
			'woocommerce_save_product_variation',
			[ $this, 'update_product_variation' ],
			100,
			2
		);
	}

	/**
	 * Output a text input box for the registration closing date.
	 *
	 * @since 1.0.0
	 * @param string $id           Input element ID attribute.
	 * @param string $name         Input element name attribute.
	 * @param string $value        Input element value attribute.
	 * @param bool   $is_variation If `true`, the input wrapper uses "variation" style classes.
	 */
	protected function output_closing_date_text_input( $id, $name, $value, $is_variation ) {

		woocommerce_wp_text_input(
			[
				'id'                => $id,
				'name'              => $name,
				'value'             => $value,
				'label'             => __( 'Closing date', 'ponder-events-fwc' ),
				'placeholder'       => 'YYYY-MM-DD',
				'description'       => __(
					'The "Add to cart" button is disabled after this date.',
					'ponder-events-fwc'
				),
				'desc_tip'          => true,
				'class'             => 'date-picker',
				'wrapper_class'     => $is_variation ? 'form-row form-row-first' : '',
				'custom_attributes' => [
					'pattern' => apply_filters(
						'woocommerce_date_input_html_pattern',
						'[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])'
					),
				],
			]
		);
	}

	/**
	 * Outputs a text input field for the closing date on a simple product edit page.
	 *
	 * @since 1.0.0
	 */
	public function output_general_options() {

		/* @var WC_Product $product_object */
		global $product_object;

		echo <<<'NOWDOC'
<div class="options_group show_if_simple">

NOWDOC;
		$value = get_post_meta(
			$product_object->get_id(),
			FrontEnd\Closing_Date::METADATA_KEY,
			true
		);
		$this->output_closing_date_text_input(
			FrontEnd\Closing_Date::METADATA_KEY,
			FrontEnd\Closing_Date::METADATA_KEY,
			$value,
			false
		);
		echo <<<'NOWDOC'
</div>

NOWDOC;
	}

	/**
	 * Outputs a text input field for the closing date on a variable product edit page.
	 *
	 * @since 1.0.0
	 * @param int     $loop           Position in the loop.
	 * @param array   $variation_data Array of variation data deprecated in WooCommerce 4.4.0.
	 * @param WP_Post $variation      Post data.
	 */
	public function output_variable_attributes( $loop, $variation_data, $variation ) {

		echo <<<'NOWDOC'
<div>

NOWDOC;
		$value = get_post_meta( $variation->ID, FrontEnd\Closing_Date::METADATA_KEY, true );
		$this->output_closing_date_text_input(
			FrontEnd\Closing_Date::METADATA_KEY . "_$loop",
			FrontEnd\Closing_Date::METADATA_KEY . "[$loop]",
			$value,
			true
		);
		echo <<<'NOWDOC'
</div>
<script type="text/javascript">
jQuery( document.body ).trigger( 'wc-init-datepickers' );
</script>

NOWDOC;
	}

	/**
	 * Saves product "Closing Date" status for a product variation.
	 *
	 * phpcs:disabled WordPress.Security.NonceVerification.Missing
	 * Nonce verified by {@see \WC_AJAX::save_variations()}
	 *
	 * @see \WC_Meta_Box_Product_Data::save_variations()
	 * @since 1.0.0
	 * @param int $variation_id The ID of the variable post.
	 * @param int $i            Position in the loop of variations.
	 */
	public function update_product_variation( $variation_id, $i ) {

		if ( ! isset( $_POST[ FrontEnd\Closing_Date::METADATA_KEY ][ $i ] ) ) {
			return;
		}
		$closing_date = sanitize_text_field(
			wp_unslash( $_POST[ FrontEnd\Closing_Date::METADATA_KEY ][ $i ] )
		);
		update_post_meta( $variation_id, FrontEnd\Closing_Date::METADATA_KEY, $closing_date );
	}

	/**
	 * Saves product "Closing Date" status for a simple product.
	 *
	 * phpcs:disabled WordPress.Security.NonceVerification.Missing
	 * Nonce verified by {@see \WC_Admin_Meta_Boxes::save_meta_boxes()}
	 *
	 * @see \WC_Meta_Box_Product_Data::save()
	 * @since 1.0.0
	 * @param int $post_id Product post ID.
	 */
	public function update_simple_product( $post_id ) {

		if ( ! isset( $_POST[ FrontEnd\Closing_Date::METADATA_KEY ] ) ) {
			return;
		}
		$closing_date = sanitize_text_field(
			wp_unslash( $_POST[ FrontEnd\Closing_Date::METADATA_KEY ] )
		);
		update_post_meta( $post_id, FrontEnd\Closing_Date::METADATA_KEY, $closing_date );
	}
}
