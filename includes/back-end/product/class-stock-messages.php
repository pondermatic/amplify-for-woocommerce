<?php
/**
 * Back end product Stock_Messages class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\BackEnd\Product;

use Pondermatic\PonderEventsForWooCommerce\Free\Traits\Hooks;

/**
 * Manages a product's stock messages.
 *
 * @since 1.0.0
 */
class Stock_Messages {

	use Hooks;
}
