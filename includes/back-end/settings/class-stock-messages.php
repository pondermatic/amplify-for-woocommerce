<?php
/**
 * Back end settings Stock_Messages class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\BackEnd\Settings;

use Pondermatic\PonderEventsForWooCommerce\Free\BackEnd\Notices\Notice;
use Pondermatic\PonderEventsForWooCommerce\Free\Core;
use Pondermatic\PonderEventsForWooCommerce\Free\FrontEnd as FrontEnd;
use WC_Admin_Settings;

/**
 * Manages stock messages settings.
 *
 * @since 1.0.0
 */
class Stock_Messages extends Section {

	/**
	 * Inventory URL.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $inventory_url;

	/**
	 * The ID of this section.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $section_id = 'stock-messages';

	/**
	 * Stock_Messages ID.
	 *
	 * @since 1.0.0
	 * @var int
	 */
	protected $settings_id = 1;

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		$this->handles_saves = true;
		$this->section_title = __( 'Stock Messages', 'ponder-events-fwc' );
		parent::__construct();
	}

	/**
	 * Adds a "Closing Date" settings section.
	 *
	 * @since 1.0.0
	 * @param array  $settings Stock messages settings.
	 * @param string $key      Option name.
	 * @return array
	 */
	public function add_closing_date_settings_section( $settings, $key ) {

		$settings['closing_date_begin']  = [
			'id'    => 'closing_date',
			'title' => __( 'Closing Date', 'ponder-events-fwc' ),
			'desc'  => __(
				'Displayed when a product has a closing date.<br><code>&#37;s</code> is replaced with the closing date.',
				'ponder-events-fwc'
			),
			'type'  => 'title',
		];
		$settings['closing_date_before'] = [
			'id'          => $key . '[closing_date_before]',
			'title'       => __( 'Today <= product closing date', 'ponder-events-fwc' ),
			/* translators: %s: formatted closing date */
			'placeholder' => __( 'Can be purchased through %s.', 'ponder-events-fwc' ),
			'type'        => 'text',
			'class'       => 'manage_stock_field',
		];
		$settings['closing_date_after']  = [
			'id'          => $key . '[closing_date_after]',
			'title'       => __( 'Today > product closing date', 'ponder-events-fwc' ),
			/* translators: %s: formatted closing date */
			'placeholder' => __( 'Can not be purchased after %s.', 'ponder-events-fwc' ),
			'type'        => 'text',
			'class'       => 'manage_stock_field',
		];
		$settings['closing_date_end']    = [
			'id'   => 'closing_date',
			'type' => 'sectionend',
		];

		return $settings;
	}

	/**
	 * Adds a "Current WooCommerce Inventory Settings" section.
	 *
	 * @since 1.0.0
	 * @param array $settings Stock messages settings.
	 * @return array
	 */
	protected function add_current_woocommerce_settings_section( $settings ) {

		$stock_format_value = get_option( 'woocommerce_stock_format' );
		switch ( $stock_format_value ) {
			case '':
				$text = __(
					'Always show quantity remaining in stock e.g. "12 in stock"',
					'woocommerce'
				);
				break;
			case 'low_amount':
				$text = __(
					'Only show quantity remaining in stock when low e.g. "Only 2 left in stock"',
					'woocommerce'
				);
				break;
			case 'no_amount':
				$text = __( 'Never show quantity remaining in stock', 'woocommerce' );
				break;
			default:
				$text = __( 'unknown value', 'ponder-events-fwc' );
		}
		$stock_display_format = sprintf(
			/* translators: %1$s: link to the "stock display format" setting %2$s: value of the "stock display format" setting */
			_x(
				'%1$s is set to: <strong>%2$s</strong>.',
				'stock display format setting',
				'ponder-events-fwc'
			),
			$this->get_stock_display_format_setting_link(),
			$text
		);

		$manage_stock_value = get_option( 'woocommerce_manage_stock' );
		$manage_stock       = sprintf(
			/* translators: %1$s: link to the "manage stock" setting %2$s: value of the "manage stock" setting */
			_x(
				'%1$s is set to: <strong>%2$s</strong>.',
				'manage stock setting',
				'ponder-events-fwc'
			),
			$this->get_manage_stock_setting_link(),
			$manage_stock_value
		);

		$settings['current_woocommerce_settings_begin'] = [
			'id'    => 'current_woocommerce_settings',
			'title' => __( 'Current WooCommerce Inventory Settings', 'ponder-events-fwc' ),
			'desc'  => $stock_display_format . "\n" . $manage_stock,
			'type'  => 'title',
		];
		$settings['current_woocommerce_settings_end']   = [
			'id'   => 'current_woocommerce_settings',
			'type' => 'sectionend',
		];

		return $settings;
	}

	/**
	 * Adds an "In Stock with Quantity" settings section.
	 *
	 * @since 1.0.0
	 * @param array  $settings Stock messages settings.
	 * @param string $key      Option name.
	 * @return array
	 */
	protected function add_in_stock_with_quantity_settings_section( $settings, $key ) {

		$settings['in_stock_with_quantity_begin'] = [
			'id'    => 'in_stock_with_quantity',
			'title' => __( 'In Stock with Quantity', 'ponder-events-fwc' ),
			'desc'  => sprintf(
				/* translators: %1$s: link to the "stock display format" setting %2$s: value of the "stock display format" setting */
				__(
					'Displayed when %1$s is set to: <strong>%2$s</strong>.<br><code>&#37;s</code> is replaced with the quantity.',
					'ponder-events-fwc'
				),
				$this->get_stock_display_format_setting_link(),
				__( 'Always show quantity remaining in stock e.g. "12 in stock"', 'woocommerce' )
			),
			'type'  => 'title',
		];
		$settings['in_stock_plural']              = [
			'id'          => $key . '[in_stock_plural]',
			'title'       => __( 'In stock quantity > 1', 'ponder-events-fwc' ),
			/* translators: %s: count */
			'placeholder' => __( '%s in stock', 'woocommerce' ),
			'type'        => 'text',
			'class'       => 'manage_stock_field',
		];
		$settings['in_stock_singular']            = [
			'id'                => $key . '[in_stock_singular]',
			'title'             => __( 'In stock quantity = 1', 'ponder-events-fwc' ),
			'desc'              => Core::get_upgrade_message(),
			'desc_tip'          => __(
				'If this field is empty, the value from "In stock quantity > 1" will be used.',
				'ponder-events-fwc'
			),
			/* translators: %s: count */
			'placeholder'       => __( '%s in stock', 'woocommerce' ),
			'type'              => 'text',
			'class'             => 'manage_stock_field',
			'custom_attributes' => [ 'readonly' => 'readonly' ],
		];
		$settings['in_stock_with_quantity_end']   = [
			'id'   => 'in_stock_with_quantity',
			'type' => 'sectionend',
		];

		return $settings;
	}

	/**
	 * Adds an "In Stock without Quantity" settings section.
	 *
	 * @since 1.0.0
	 * @param array  $settings Stock messages settings.
	 * @param string $key      Option name.
	 * @return array
	 */
	protected function add_in_stock_without_quantity_settings_section( $settings, $key ) {

		$settings['in_stock_without_quantity_begin'] = [
			'id'    => 'in_stock_without_quantity',
			'title' => __( 'In Stock without Quantity', 'ponder-events-fwc' ),
			'desc'  => sprintf(
				/* translators: %1$s: link to the "stock display format" setting %2$s: "stock display format" setting value */
				__(
					'Displayed when %1$s is set to: <strong>%2$s</strong>.',
					'ponder-events-fwc'
				),
				$this->get_stock_display_format_setting_link(),
				__( 'Never show quantity remaining in stock', 'woocommerce' )
			),
			'type'  => 'title',
		];
		$settings['in_stock']                        = [
			'id'          => $key . '[in_stock]',
			'title'       => __( 'In stock', 'ponder-events-fwc' ),
			'placeholder' => __( 'In stock', 'woocommerce' ),
			'type'        => 'text',
			'class'       => 'manage_stock_field',
		];
		$settings['in_stock_without_quantity_end']   = [
			'id'   => 'in_stock_without_quantity',
			'type' => 'sectionend',
		];

		return $settings;
	}

	/**
	 * Adds a "Low Stock with Quantity" settings section.
	 *
	 * @since 1.0.0
	 * @param array  $settings Stock messages settings.
	 * @param string $key      Option name.
	 * @return array
	 */
	protected function add_low_stock_with_quantity_settings_section( $settings, $key ) {

		$settings['low_stock_begin']    = [
			'id'    => 'low_stock',
			'title' => __( 'Low Stock with Quantity', 'ponder-events-fwc' ),
			'desc'  => sprintf(
				/* translators: %1$s: link to the "stock display format" setting %2$s: "stock display format" setting value %3$s: link to the "low stock threshold" setting */
				__(
					'Displayed when %1$s is set to: <strong>%2$s</strong> and the quantity is less than or equal to %3$s.<br><code>&#37;s</code> is replaced with the quantity.',
					'ponder-events-fwc'
				),
				$this->get_stock_display_format_setting_link(),
				__(
					'Only show quantity remaining in stock when low e.g. "Only 2 left in stock"',
					'woocommerce'
				),
				$this->get_low_stock_threshold_setting_link()
			),
			'type'  => 'title',
		];
		$settings['low_stock_plural']   = [
			'id'          => $key . '[low_stock_plural]',
			'title'       => __( 'Low stock quantity > 1', 'ponder-events-fwc' ),
			/* translators: %s: count */
			'placeholder' => __( 'Only %s left in stock', 'woocommerce' ),
			'type'        => 'text',
			'class'       => 'manage_stock_field',
		];
		$settings['low_stock_singular'] = [
			'id'                => $key . '[low_stock_singular]',
			'title'             => __( 'Low stock quantity = 1', 'ponder-events-fwc' ),
			'desc'              => Core::get_upgrade_message(),
			'desc_tip'          => __(
				'If this field is empty, the value from "Low stock quantity > 1" will be used.',
				'ponder-events-fwc'
			),
			/* translators: %s: count */
			'placeholder'       => __( 'Only %s left in stock', 'woocommerce' ),
			'type'              => 'text',
			'class'             => 'manage_stock_field',
			'custom_attributes' => [ 'readonly' => 'readonly' ],
			'autoload'          => false, // Only needs set on the last input element if using an [array] ID/name.
		];
		$settings['low_stock_end']      = [
			'id'   => 'low_stock',
			'type' => 'sectionend',
		];

		return $settings;
	}

	/**
	 * Add notices.
	 *
	 * @since 1.0.0
	 * @param string $notice_type The type of notice to display: 'created'|'deleted'|'updated'.
	 * @param array  $data        Additional data used in the notice.
	 */
	protected function add_notice( $notice_type, $data = array() ) {

		$notices = Core::get_notices_instance();

		switch ( $notice_type ) {
			case 'created':
				$notices->add_notice(
					__(
						'This set of stock messages has been successfully created.',
						'ponder-events-fwc'
					),
					Notice::TYPE_SUCCESS
				);
				break;
			case 'updated':
				$notices->add_notice(
					__(
						'This set of stock messages has been successfully updated.',
						'ponder-events-fwc'
					),
					Notice::TYPE_SUCCESS
				);
				break;
		}
	}

	/**
	 * Adds a "Stock Messages" settings section.
	 *
	 * @since 1.0.0
	 * @param array  $settings Stock messages settings.
	 * @param string $key      Option name.
	 * @return array
	 */
	protected function add_stock_messages_settings_section( $settings, $key ) {

		$settings['stock_messages_begin']   = [
			'id'    => 'pefwc_stock_messages',
			'title' => __( 'Stock Messages', 'ponder-events-fwc' ),
			'type'  => 'title',
			'value' => '', // Prevent WC_Admin_Settings::get_option() from calling get_option().
		];
		$settings['enabled']                = [
			'id'       => $key . '[enabled]',
			'title'    => __( 'Custom stock messages', 'ponder-events-fwc' ),
			'desc'     => __( 'Enable', 'ponder-events-fwc' ),
			'desc_tip' => __(
				'If unchecked, WooCommerce messages will be used.',
				'ponder-events-fwc'
			),
			'default'  => 'yes',
			'type'     => 'checkbox',
			'class'    => 'manage_stock_field',
		];
		$settings['name']                   = [
			'id'                => $key . '[name]',
			'title'             => 'Name',
			'desc'              => Core::get_upgrade_message(),
			'desc_tip'          => __(
				'Friendly name for this set of stock messages.',
				'ponder-events-fwc'
			),
			'default'           => __( 'Custom stock messages', 'ponder-events-fwc' ),
			'placeholder'       => __( 'Custom stock messages', 'ponder-events-fwc' ),
			'type'              => 'text',
			'class'             => 'manage_stock_field',
			'custom_attributes' => [ 'readonly' => 'readonly' ],
		];
		$settings['available_on_backorder'] = [
			'id'          => $key . '[available_on_backorder]',
			'title'       => __( 'Available on backorder', 'ponder-events-fwc' ),
			'placeholder' => __( 'Available on backorder', 'woocommerce' ),
			'type'        => 'text',
			'class'       => 'manage_stock_field',
		];
		$settings['can_be_backordered']     = [
			'id'          => $key . '[can_be_backordered]',
			'title'       => __( 'Can be backordered', 'ponder-events-fwc' ),
			'placeholder' => __( '(can be backordered)', 'woocommerce' ),
			'type'        => 'text',
			'class'       => 'manage_stock_field',
		];
		$settings['out_of_stock']           = [
			'id'          => $key . '[out_of_stock]',
			'title'       => __( 'Out of stock', 'ponder-events-fwc' ),
			'placeholder' => __( 'Out of stock', 'woocommerce' ),
			'type'        => 'text',
			'class'       => 'manage_stock_field',
		];
		$settings['stock_messages_end']     = [
			'id'    => 'pefwc_stock_messages',
			'type'  => 'sectionend',
			'value' => '', // Prevent WC_Admin_Settings::get_option() from calling get_option().
		];

		return $settings;
	}

	/**
	 * Returns the URL of the WooCommerce -> Settings -> Products -> Inventory page.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_inventory_url() {

		if ( empty( $this->inventory_url ) ) {
			$this->inventory_url = get_admin_url()
				. 'admin.php?page=wc-settings&tab=products&section=inventory';
		}
		return $this->inventory_url;
	}

	/**
	 * Returns an HTML link to the WooCommerce settings page with the
	 * product inventory "Low stock threshold" setting.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_low_stock_threshold_setting_link() {

		/* @noinspection HtmlUnknownTarget */
		return sprintf(
			'<a href="%1$s">%2$s</a>',
			$this->get_inventory_url() . '#woocommerce_notify_low_stock_amount',
			__( 'Low stock threshold', 'woocommerce' )
		);
	}

	/**
	 * Returns an HTML link to the WooCommerce settings page with the
	 * product inventory "Manage stock" setting.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_manage_stock_setting_link() {

		/* @noinspection HtmlUnknownTarget */
		return sprintf(
			'<a href="%1$s">%2$s</a>',
			$this->get_inventory_url() . '#woocommerce_manage_stock',
			__( 'Manage stock', 'woocommerce' )
		);
	}

	/**
	 * Returns an array of stock messages settings.
	 *
	 * @see WC_Admin_Settings::output_fields()
	 * @since 1.0.0
	 * @return array
	 */
	public function get_settings() {

		if ( ! current_user_can( 'manage_woocommerce' ) ) {
			return [];
		}

		$key = FrontEnd\Stock_Messages::POST_KEY_PREFIX . $this->settings_id;

		$settings = $this->add_stock_messages_settings_section( [], $key );
		$settings = $this->add_current_woocommerce_settings_section( $settings );
		$settings = $this->add_in_stock_with_quantity_settings_section( $settings, $key );
		$settings = $this->add_in_stock_without_quantity_settings_section( $settings, $key );
		$settings = $this->add_low_stock_with_quantity_settings_section( $settings, $key );
		$settings = $this->add_closing_date_settings_section( $settings, $key );

		return $settings;
	}

	/**
	 * Returns an HTML link to the WooCommerce settings page with the
	 * product inventory "Stock display format" setting.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_stock_display_format_setting_link() {

		/* @noinspection HtmlUnknownTarget */
		return sprintf(
			'<a href="%1$s">%2$s</a>',
			$this->get_inventory_url() . '#woocommerce_stock_format',
			__( 'Stock display format', 'woocommerce' )
		);
	}

	/**
	 * Replaces empty posted values with default settings values.
	 *
	 * @see WC_Admin_Settings::save_fields()
	 * @since 1.0.0
	 * @param mixed       $value     Option value.
	 * @param array       $option    {
	 *     Option field.
	 *
	 *     @type string $default Default value.
	 * }
	 * @param string|null $raw_value Not used.
	 * @return mixed
	 */
	public function sanitize_option( $value, $option, $raw_value ) {

		if ( $value === '' && isset( $option['default'] ) ) {
			$value = $option['default'];
		}

		return $value;
	}

	/**
	 * Saves the stock messages settings and exits.
	 *
	 * @since 1.0.0
	 */
	protected function save_settings() {

		add_filter(
			'woocommerce_admin_settings_sanitize_option',
			[ $this, 'sanitize_option' ],
			10,
			3
		);

		$settings = $this->get_settings();
		WC_Admin_Settings::save_fields( $settings );

		check_admin_referer( 'woocommerce-settings' );
		$notice_type = isset( $_REQUEST['set'] ) && absint( $_REQUEST['set'] ) === 0 ? 'created' : 'updated';
		$this->add_notice( $notice_type );

		wp_safe_redirect( add_query_arg( 'set', $this->settings_id ) );
		exit();
	}
}
