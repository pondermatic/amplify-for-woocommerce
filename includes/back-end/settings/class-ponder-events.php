<?php
/**
 * Back end settings Ponder_Events class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\BackEnd\Settings;

use Pondermatic\PonderEventsForWooCommerce\Free\Core;
use WC_Settings_Page;

/**
 * The WooCommerce -> Settings -> Events settings page.
 *
 * @since 1.0.0
 */
class Ponder_Events extends WC_Settings_Page {

	/**
	 * Setting page sections.
	 *
	 * @since 1.0.0
	 * @var Section[] $sections {
	 *     Section settings indexed by section ID.
	 *     @type Products       $products
	 *     @type Stock_Messages $stock_messages
	 * }
	 */
	protected $sections = [];

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		$this->id    = 'ponder-events';
		$this->label = __( 'Events', 'ponder-events-fwc' );

		Core::get_notices_instance();

		parent::__construct();
		$this->initialize_sections();

		add_filter(
			'admin_footer_text',
			[ $this, 'admin_footer_text' ],
			10,
			1
		);
	}

	/**
	 * Replaces the footer text on stock messages admin pages.
	 *
	 * @since 1.0.0
	 * @param string $text Current footer text.
	 * @return string
	 */
	public function admin_footer_text( $text ) {

		// phpcs:disabled WordPress.Security.NonceVerification.Recommended
		if ( empty( $_GET['tab'] ) || $_GET['tab'] !== $this->id ) {
			return $text;
		}

		$text = sprintf(
			/* translators: %s: plugin name */
			__( 'Thank you for using <strong>%s</strong>!', 'ponder-events-fwc' ),
			Core::get_plugin_name()
		);

		if ( Core::is_pro() === false ) {
			$text .= sprintf(
				/* translators: %1$s: five star rating URL */
				__(
					' Please leave us a %1$s rating.',
					'ponder-events-fwc'
				),
				Core::get_plugin_rating_link_free()
			);
		}

		return "<span id='footer-thankyou'>$text</span>";
	}

	/**
	 * Get sections for section navigation in this settings tab.
	 *
	 * @since 1.0.0
	 * @return array [ $section_id => $translated_section_name ].
	 */
	protected function get_own_sections() {

		$sections = [];
		foreach ( $this->sections as $section_id => $section ) {
			$sections[ $section_id ] = $section->get_section_title();
		}

		return $sections;
	}

	/**
	 * Get the settings for a given section.
	 *
	 * @since 1.0.0
	 * @param string $section_id The section name to get the settings for.
	 * @return array
	 */
	protected function get_settings_for_section_core( $section_id ) {

		if ( array_key_exists( $section_id, $this->sections ) ) {
			return $this->sections[ $section_id ]->get_settings();
		}

		return [];
	}

	/**
	 * Instantiates settings section objects.
	 *
	 * @since 1.0.0
	 */
	public function initialize_sections() {

		$this->sections['stock-messages'] = new Stock_Messages();
		$this->sections['products']       = new Products();
	}

	/**
	 * Output the settings.
	 *
	 * Patterned off of {@see WC_Settings_Advanced::output()}.
	 *
	 * @since 1.0.0
	 */
	public function output() {

		global $current_section;

		if ( $current_section === '' ) {
			$this->sections['stock-messages']->output();

		} elseif ( array_key_exists( $current_section, $this->sections ) ) {
			$this->sections[ $current_section ]->output();

		} else {
			parent::output();
		}
	}
}
