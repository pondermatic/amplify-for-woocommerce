<?php
/**
 * Section class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\BackEnd\Settings;

use WC_Admin_Settings;

/**
 * A settings tab's section.
 *
 * @since 1.0.0
 */
abstract class Section {

	/**
	 * True if this section handles saving its own settings,
	 * else false to let WC_Admin_Settings::save() do it.
	 *
	 * @since 1.0.0
	 * @var bool
	 */
	protected $handles_saves = false;

	/**
	 * The ID of this section.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $section_id;

	/**
	 * The translated title for this section.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $section_title;

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		// Handle a submitted form on the WooCommerce -> Settings -> Events -> SECTION page.
		/* @see WC_Admin_Menus::save_settings() */
		if ( $this->handles_saves ) {
			add_filter(
				"woocommerce_save_settings_ponder-events_{$this->section_id}",
				[ $this, 'maybe_handle_save' ],
				10,
				1
			);
			add_filter(
				'woocommerce_save_settings_ponder-events',
				[ $this, 'maybe_handle_save' ],
				10,
				1
			);
		}
	}

	/**
	 * Returns the translated section title.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_section_title() {

		return $this->section_title;
	}

	/**
	 * Returns an array of settings for the settings tab's section.
	 *
	 * @see WC_Admin_Settings::output_fields()
	 * @since 1.0.0
	 * @return array
	 */
	abstract public function get_settings();

	/**
	 * Returns `true` if it is safe to save this section's settings, else `false`.
	 *
	 * @since 1.0.0
	 * @param bool $is_save_data_posted `true` if data to save is posted, else `false`.
	 * @return bool
	 */
	protected function is_safe_to_save( $is_save_data_posted ) {

		if (
			$is_save_data_posted === false ||
			$this->is_settings_page() === false ||
			check_admin_referer( 'woocommerce-settings' ) === false
		) {
			return false;
		}

		if ( ! current_user_can( 'manage_woocommerce' ) ) {
			WC_Admin_Settings::add_message(
				__(
					'You do not have permission to edit settings.',
					'ponder-events-fwc'
				)
			);
			return false;
		}
		return true;
	}

	/**
	 * Returns true if the request is for this settings page, tab and section.
	 *
	 * @since 1.0.0
	 * @return bool
	 */
	protected function is_settings_page() {

		$request = filter_input_array( INPUT_GET, FILTER_SANITIZE_FULL_SPECIAL_CHARS );
		return isset( $request['page'], $request['tab'] ) &&
			$request['page'] === 'wc-settings' &&
			$request['tab'] === 'ponder-events' &&
			( empty( $request['section'] ) || $request['section'] === $this->section_id );
	}

	/**
	 * Saves this plugin's settings, if it is OK to do so, then returns
	 * `false` to prevent {@see WC_Admin_Settings::save()} from being called.
	 *
	 * Extended classes should override this method to add other non-save actions
	 * such as 'delete'.
	 *
	 * Flow:
	 * 1. 'wp_loaded' action hook
	 * 2. {@see WC_Admin_Menus::save_settings()}
	 * 3. "woocommerce_save_settings_{$current_tab}_{$current_section}" filter hook
	 * 4. {@see Section::maybe_handle_save()}
	 *
	 * @since 1.0.0
	 * @param bool $is_save_data_posted `true` if data to save is posted, else `false`.
	 * @return bool
	 */
	public function maybe_handle_save( $is_save_data_posted ) {

		if ( $this->is_safe_to_save( $is_save_data_posted ) ) {
			$this->save_settings();
		}
		return false;
	}

	/**
	 * Output the HTML for the section's settings.
	 *
	 * @since 1.0.0
	 */
	public function output() {

		$settings = $this->get_settings();
		WC_Admin_Settings::output_fields( $settings );
	}

	/**
	 * Saves this section's settings.
	 *
	 * Extended classes that handle their own saves should override this class
	 * and set the {@see self::$handles_saves} property to `true`.
	 * Otherwise, {@see WC_Admin_Settings::save()} will be called.
	 *
	 * @since 1.0.0
	 */
	protected function save_settings() {}
}
