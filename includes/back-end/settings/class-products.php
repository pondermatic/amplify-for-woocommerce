<?php
/**
 * Back end settings Products class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\BackEnd\Settings;

use Pondermatic\PonderEventsForWooCommerce\Free\FrontEnd\Sku;
use WC_Admin_Settings;

/**
 * Manages how the product is displayed.
 *
 * @since 1.0.0
 */
class Products extends Section {

	/**
	 * The ID of this section.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $section_id = 'products';

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		$this->section_title = __( 'Products', 'woocommerce' );
		parent::__construct();
	}

	/**
	 * Returns an array of shop pages settings.
	 *
	 * @see WC_Admin_Settings::output_fields()
	 * @since 1.0.0
	 * @return array
	 */
	public function get_settings() {

		$settings = $this->get_shop_pages_section( [] );
		return $settings;
	}

	/**
	 * Adds a "Shop pages" section to the "Products" section.
	 *
	 * @since 1.0.0
	 * @param array $settings Shop pages settings.
	 * @return array
	 */
	protected function get_shop_pages_section( $settings ) {

		$settings[] = [
			'title' => __( 'Shop pages', 'woocommerce' ),
			'type'  => 'title',
			'desc'  => '',
			'id'    => 'catalog_options',
		];
		$settings[] = [
			'title'    => __( 'SKU', 'ponder-events-fwc' ),
			'desc'     => __( 'Do not display SKU if empty', 'ponder-events-fwc' ),
			'desc_tip' => __(
				'Prevents "SKU: N/A" from being displayed for variable products if all variations do not have a Stock Keeping Unit.',
				'ponder-events-fwc'
			),
			'id'       => Sku::OPTION_NAME,
			'default'  => 'no',
			'type'     => 'checkbox',
		];
		$settings[] = [
			'type' => 'sectionend',
			'id'   => 'catalog_options',
		];

		return $settings;
	}
}
