<?php
/**
 * Back end Notice class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\BackEnd\Notices;

use JsonSerializable;
use stdClass;

/**
 * An administrator notice.
 *
 * @since 1.0.0
 */
class Notice implements JsonSerializable {

	/**
	 * Error notice type.
	 *
	 * @since 1.0.0
	 */
	public const TYPE_ERROR = 0;

	/**
	 * Info notice type.
	 *
	 * @since 1.0.0
	 */
	public const TYPE_INFO = 1;

	/**
	 * Success notice type.
	 *
	 * @since 1.0.0
	 */
	public const TYPE_SUCCESS = 2;

	/**
	 * Warning notice type.
	 *
	 * @since 1.0.0
	 */
	public const TYPE_WARNING = 3;

	/**
	 * The translated message to display to administrators.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $message;

	/**
	 * The notice type.
	 *
	 * One of the {@see Notice::TYPE_X} constants.
	 *
	 * @since 1.0.0
	 * @var int
	 */
	protected $type;

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 * @param string $message The translated message to display to administrators.
	 * @param int    $type    One of the Notice::TYPE_X constants.
	 */
	public function __construct( $message, $type = self::TYPE_INFO ) {

		$this->set_message( $message );
		$this->set_type( $type );
	}

	/**
	 * Returns the WordPress CSS class name for the given notice type.
	 *
	 * @since 1.0.0
	 * @param int $type One of the Notice::TYPE_X constants. Defaults to $this->type
	 *                  and then {@see self::TYPE_INFO}.
	 * @return string
	 */
	public function get_css_class( $type = null ) {

		if ( is_null( $type ) ) {
			$type = $this->type;
		}

		switch ( $type ) {
			case self::TYPE_ERROR:
				$class = 'notice-error';
				break;
			case self::TYPE_INFO:
				$class = 'notice-info';
				break;
			case self::TYPE_SUCCESS:
				$class = 'notice-success';
				break;
			case self::TYPE_WARNING:
				$class = 'notice-warning';
				break;
			default:
				$class = 'notice-info';
		}

		return $class;
	}

	/**
	 * Returns the translated message.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_message() {

		return $this->message;
	}

	/**
	 * Returns the notice type.
	 *
	 * @since 1.0.0
	 * @return int One of the Notice::TYPE_X constants.
	 */
	public function get_type() {

		return $this->type;
	}

	/**
	 * Serializes the object to a value that can be serialized natively by json_encode().
	 *
	 * @since 1.0.0
	 * @return array
	 */
	public function jsonSerialize() {

		return [
			'message' => $this->message,
			'type'    => $this->type,
		];
	}

	/**
	 * Returns a Notice object using a given stdClass object.
	 *
	 * @since 1.0.0
	 * @param stdClass $object A Notice object as a stdClass object.
	 * @return Notice
	 */
	public static function new_with_object( $object ) {

		return new Notice( $object->message ?? null, $object->type ?? null );
	}

	/**
	 * Sets the translated message.
	 *
	 * @since 1.0.0
	 * @param string $message The translated message to display to administrators.
	 * @return $this
	 */
	public function set_message( $message ) {

		$this->message = $message;
		return $this;
	}

	/**
	 * Sets the notice type.
	 *
	 * @since 1.0.0
	 * @param int $type One of the Notice::TYPE_X constants.
	 * @return $this
	 */
	public function set_type( $type ) {

		$this->type = $type;
		return $this;
	}
}
