<?php
/**
 * Back end Notices class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\BackEnd\Notices;

/**
 * Manages administrator notices.
 *
 * @since 1.0.0
 */
class Notices {

	/**
	 * An array of notices.
	 *
	 * @since 1.0.0
	 * @var Notice[]
	 */
	protected $notices = [];

	/**
	 * The name of the option in the options database table to save the notices to.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $option_name;
	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		$this->option_name = 'pefwc_notices_user_' . get_current_user_id();
		$this->load_notices();
		add_action( 'admin_notices', [ $this, 'output_notices' ], 10, 0 );
		add_action( 'shutdown', [ $this, 'save_notices' ], 10, 0 );
	}

	/**
	 * Adds a notice to the array of notices.
	 *
	 * @since 1.0.0
	 * @param string $message The message to display.
	 * @param int    $type    One of the Notice::TYPE_X constants.
	 */
	public function add_notice( $message, $type = Notice::TYPE_INFO ) {

		$this->notices[] = new Notice( $message, $type );
	}

	/**
	 * Deletes the user's administrator notices.
	 *
	 * @since 1.0.0
	 */
	protected function delete_notices() {

		delete_option( $this->option_name );
		$this->notices = [];
	}

	/**
	 * Loads notices from the options database table.
	 *
	 * @since 1.0.0
	 */
	public function load_notices() {

		$notices = json_decode(
			get_option( $this->option_name, '[]' )
		);
		foreach ( $notices as $notice ) {
			$this->notices[] = Notice::new_with_object( $notice );
		}
	}

	/**
	 * Display the administrator notices.
	 *
	 * @since 1.0.0
	 */
	public function output_notices() {

		foreach ( $this->notices as $notice ) {
			printf(
				"<div class='notice %s is-dismissible'><p>%s</p></div>\n",
				esc_attr( $notice->get_css_class() ),
				esc_html( $notice->get_message() )
			);
		}

		$this->delete_notices();
	}

	/**
	 * Save any remaining notices to the options database table.
	 *
	 * @since 1.0.0
	 */
	public function save_notices() {

		if ( empty( $this->notices ) ) {
			return;
		}

		update_option( $this->option_name, wp_json_encode( $this->notices ) );
	}
}
