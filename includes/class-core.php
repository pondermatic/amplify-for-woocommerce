<?php
/**
 * Core class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free;

use Pondermatic\PonderEventsForWooCommerce\Free\BackEnd\Notices\Notices;
use Pondermatic\PonderEventsForWooCommerce\Free\BackEnd\Settings\Ponder_Events;
use WC_Settings_Page;

/**
 * The core plugin class.
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since 1.0.0
 */
class Core {

	/**
	 * The version number of this plugin.
	 *
	 * @since 1.0.0
	 */
	public const VERSION = '1.0.0';

	/**
	 * Is this a premium plugin edition?
	 *
	 * @since 1.0.0
	 * @var bool
	 */
	protected static $is_pro = false;

	/**
	 * The root namespace of this plugin.
	 *
	 * This allows free classes to use pro classes.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected static $namespace = __NAMESPACE__;

	/**
	 * A singleton instance of the back end Notices object.
	 *
	 * @since 1.0.0
	 * @var Notices
	 */
	protected static $notices;

	/**
	 * The basename of this plugin.
	 *
	 * Plugin slug plus main file, e.g. 'ponder-events-for-woocommerce/ponder-events-for-woocommerce.php'.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected static $plugin_basename;

	/**
	 * The name of this plugin.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected static $plugin_name;

	/**
	 * The URL for the free plugin.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected static $plugin_url_free = 'https://www.pondermatic.com/ponder-events-for-woocommerce/';

	/**
	 * The URL for the premium plugin.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected static $plugin_url_pro = 'https://www.pondermatic.com/product/ponder-events-pro-for-woocommerce/';

	/**
	 * A singleton instance of the Ponder_Events back end settings object.
	 *
	 * @since 1.0.0
	 * @var Ponder_Events
	 */
	protected static $settings;

	/**
	 * Upgrade message.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected static $upgrade_message;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Define the locale and set the hooks for the admin area and the public-facing side of the site.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		$this->set_static_variables();
		$this->add_hooks();
	}

	/**
	 * Add hook callbacks for use on admin or public-facing pages.
	 *
	 * @since 1.0.0
	 */
	protected function add_hooks() {

		add_action( 'plugins_loaded', [ $this, 'load_plugin_textdomain' ] );

		// Allow pro classes to replace the free classes.
		// __NAMESPACE__ is the namespace of this class and not the namespace
		// from classes extended from this class.
		$namespace = substr( get_class( $this ), 0, strrpos( get_class( $this ), '\\' ) );

		if ( is_admin() ) {
			if ( __NAMESPACE__ === $namespace ) {
				( new BackEnd\Product\Stock_Messages() )->add_admin_hooks();
			}

			( new BackEnd\Product\Closing_Date() )->add_admin_hooks();
			add_filter(
				'woocommerce_get_settings_pages',
				[ $this, 'add_settings_page' ],
				1,
				1
			);
		} else {
			( new FrontEnd\Closing_Date() )->add_public_hooks();
			( new FrontEnd\Sku() )->add_public_hooks();
			if ( __NAMESPACE__ === $namespace ) {
				( new FrontEnd\Stock_Messages() )->add_public_hooks();
			}
		}
	}

	/**
	 * Adds the "Events" tab to the WooCommerce -> Settings page.
	 *
	 * @since 1.0.0
	 * @param WC_Settings_Page[] $settings An array of setting page objects.
	 * @return WC_Settings_Page[]
	 */
	public function add_settings_page( $settings ) {

		$settings[] = static::get_settings_instance();
		return $settings;
	}

	/**
	 * Returns the root namespace of this plugin.
	 *
	 * Used by free classes to use pro classes.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public static function get_namespace() {

		return self::$namespace;
	}

	/**
	 * Returns a singleton instance of the back end Notices object.
	 *
	 * @since 1.0.0
	 * @return Notices
	 */
	public static function get_notices_instance() {

		if ( empty( self::$notices ) ) {
			self::$notices = new BackEnd\Notices\Notices();
		}
		return self::$notices;
	}

	/**
	 * Returns the basename of this plugin.
	 *
	 * For example, 'ponder-events-for-woocommerce/ponder-events-for-woocommerce.php'.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public static function get_plugin_basename() {

		return self::$plugin_basename;
	}

	/**
	 * Returns a link to the premium version.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public static function get_plugin_link_pro() {

		/* @noinspection HtmlUnknownTarget */
		return sprintf(
			'<a href="%1$s" target="_blank">%2$s</a>',
			self::get_plugin_url_pro(),
			self::get_plugin_name_pro()
		);
	}

	/**
	 * Returns the name of this plugin.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public static function get_plugin_name() {

		return self::$plugin_name;
	}

	/**
	 * Returns the name of the premium plugin.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public static function get_plugin_name_pro() {

		return __(
			'Ponder Events Pro for WooCommerce',
			'ponder-events-fwc'
		);
	}

	/**
	 * Returns a link to rate the free version on this plugin.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public static function get_plugin_rating_link_free() {

		/* @noinspection HtmlUnknownTarget */
		return sprintf(
			'<a href="%1$s" target="_blank">%2$s</a>',
			self::get_plugin_url_free() . 'reviews/?rate=5#new-post',
			'&#9733;&#9733;&#9733;&#9733;&#9733;'
		);
	}

	/**
	 * Returns the URL of the free plugin.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public static function get_plugin_url_free() {

		return self::$plugin_url_free;
	}

	/**
	 * Returns the URL of the premium plugin.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public static function get_plugin_url_pro() {

		return self::$plugin_url_pro;
	}

	/**
	 * Returns a singleton instance of the Ponder_Events settings object.
	 *
	 * @since 1.0.0
	 * @return Ponder_Events
	 */
	public static function get_settings_instance() {

		if ( empty( self::$settings ) ) {
			self::$settings = new BackEnd\Settings\Ponder_Events();
		}
		return self::$settings;
	}

	/**
	 * Returns an upgrade message.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public static function get_upgrade_message() {

		return self::$upgrade_message;
	}

	/**
	 * Returns true if this plugin is the premium version.
	 *
	 * @since 1.0.0
	 * @return bool
	 */
	public static function is_pro() {

		return self::$is_pro;
	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since 1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'ponder-events-fwc',
			false,
			dirname( plugin_basename( __FILE__ ), 2 ) . '/languages/'
		);
	}

	/**
	 * Sets the basename of this plugin.
	 *
	 * For example, 'ponder-events-for-woocommerce/ponder-events-for-woocommerce.php'.
	 *
	 * @since 1.0.0
	 * @param string $plugin_basename The basename of this plugin.
	 */
	public static function set_plugin_basename( $plugin_basename ) {

		self::$plugin_basename = $plugin_basename;
	}

	/**
	 * Sets the static variables for this edition of the plugin.
	 *
	 * @since 1.0.0
	 */
	protected function set_static_variables() {

		self::$is_pro          = false;
		self::$plugin_name     = __(
			'Ponder Events for WooCommerce',
			'ponder-events-fwc'
		);
		self::$upgrade_message = (
			sprintf(
				/* translators: %s: upgrade link */
				__( 'Upgrade to %s to change value.', 'ponder-events-fwc' ),
				$this->get_plugin_link_pro()
			)
		);
	}
}
