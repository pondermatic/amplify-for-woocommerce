<?php
/**
 * Front end Closing_Date class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\FrontEnd;

use DateTime;
use Exception;
use Pondermatic\PonderEventsForWooCommerce\Free\Core;
use Pondermatic\PonderEventsForWooCommerce\Free\Models as Models;
use Pondermatic\PonderEventsForWooCommerce\Free\Traits\Hooks;
use WC_Product;
use WC_Product_Variable;
use WC_Product_Variation;

/**
 * Conditionally disables the WooCommerce "Add to cart" button after the product's closing date.
 *
 * @since 1.0.0
 */
class Closing_Date {

	use Hooks;

	/**
	 * The name of the post meta key for the product's closing date.
	 *
	 * @since 1.0.0
	 */
	public const METADATA_KEY = 'pefwc_product_closing_date';

	/**
	 * Add hook callbacks for use on public-facing pages.
	 *
	 * @since 1.0.0
	 */
	public function add_public_hooks() {

		// Display single product page.
		add_action(
			'woocommerce_after_add_to_cart_button',
			[ $this, 'disable_add_to_cart_button' ],
			100,
			0
		);
		add_filter(
			'woocommerce_available_variation',
			[ $this, 'filter_available_variation' ],
			100,
			3
		);
		add_filter(
			'woocommerce_get_availability',
			[ $this, 'filter_availability' ],
			10,
			2
		);
	}

	/**
	 * Disables the "Add to cart" button if today is after the closing date
	 * on a simple product page.
	 *
	 * @since 1.0.0
	 */
	public function disable_add_to_cart_button() {

		/* @var WC_Product $product */
		global $product;

		if ( ! $product->is_type( 'simple' ) ) {
			return;
		}
		$close_date = $this->get_product_close_date( $product );
		if ( ! $this->is_closed( $close_date ) ) {
			return;
		}

		echo <<<'NOWDOC'
<script type="text/javascript">
jQuery( '[name="add-to-cart"]' ).addClass( 'disabled' ).prop( 'disabled', true );
</script>
NOWDOC;
	}

	/**
	 * Returns an array of data for a variation. Used in the add to cart form.
	 *
	 * @noinspection PhpUnusedParameterInspection
	 * @see WC_Product_Variable::get_available_variation()
	 * @since 1.0.0
	 * @param array                $args      An array of data for a variation.
	 * @param WC_Product_Variable  $product   Variable product object.
	 * @param WC_Product_Variation $variation Variation product object.
	 * @return array
	 */
	public function filter_available_variation( $args, $product, $variation ) {

		$close_date = $this->get_product_close_date( $variation );

		if ( $this->is_closed( $close_date ) ) {
			$args['is_purchasable'] = false;
		}

		return $args;
	}

	/**
	 * Get availability text and CSS class(es) based on stock status and closing date.
	 *
	 * @see WC_Product::get_availability()
	 * @since 1.0.0
	 * @param string[]   $availability {
	 *     Availability information.
	 *     @type string $availability Text.
	 *     @type string $class        CSS class(es).
	 * }
	 * @param WC_Product $product      Product object.
	 * @return array
	 */
	public function filter_availability( $availability, $product ) {

		$close_date = $this->get_product_close_date( $product );
		if ( empty( $close_date ) || ! $product->is_in_stock() ) {
			return $availability;
		}

		/* @var Models\Stock_Messages $stock_messages_model */
		$stock_messages_model = Core::get_namespace() . '\Models\Stock_Messages';
		$options              = $stock_messages_model::fetch_with_product( $product );
		if ( $options === false ) {
			$options = [];
		}

		if ( $this->is_closed( $close_date ) ) {

			if ( empty( $options['closing_date_after'] ) ) {
				/* translators: %s: formatted closing date */
				$format = __( 'Can not be purchased after %s.', 'ponder-events-fwc' );
			} else {
				$format = $options['closing_date_after'];
			}
			$class = 'out-of-stock';

		} else {
			$format = $availability['availability'] . ' ';

			if ( empty( $options['closing_date_before'] ) ) {
				/* translators: %s: formatted closing date */
				$format .= __( 'Can be purchased through %s.', 'ponder-events-fwc' );
			} else {
				$format .= $options['closing_date_before'];
			}
			$class = 'in-stock';
		}

		$formatted_date = $close_date->format( get_option( 'date_format', 'F j, Y' ) );
		$text           = sprintf( $format, $formatted_date );
		return [
			'availability' => $text,
			'class'        => $class,
		];
	}

	/**
	 * Returns the product closing date.
	 *
	 * @since 1.0.0
	 * @param WC_Product $product Product object.
	 * @return DateTime|null
	 */
	protected function get_product_close_date( $product ) {

		$close_date = $product->get_meta( self::METADATA_KEY );

		if ( empty( $close_date ) ) {
			return null;
		}

		try {
			$close_date = new DateTime( $close_date, wp_timezone() );
		} catch ( Exception $exception ) {
			return null;
		}

		return $close_date;
	}

	/**
	 * Returns true if the current date is after the closing date.
	 *
	 * @since 1.0.0
	 * @param DateTime $close_date The date the product closes.
	 * @return bool
	 */
	protected function is_closed( $close_date ) {

		if ( empty( $close_date ) ) {
			return false;
		}

		$today = date_create( 'today', wp_timezone() );
		return $today > $close_date;
	}
}
