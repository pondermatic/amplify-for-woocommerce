<?php
/**
 * Front end Sku class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\FrontEnd;

use Pondermatic\PonderEventsForWooCommerce\Free\Traits\Hooks;
use WC_Product;
use WC_Product_Variable;
use WC_Product_Variation;

/**
 * Changes how the Stock Keeping Unit is displayed.
 *
 * @since 1.0.0
 */
class Sku {

	use Hooks;

	/**
	 * The name of the option that disables the display of empty SKU values.
	 */
	public const OPTION_NAME = 'pefwc_disable_empty_sku';

	/**
	 * Add hook callbacks for use on public-facing pages.
	 *
	 * @noinspection PhpRedundantOptionalArgumentInspection
	 * @since 1.0.0
	 */
	public function add_public_hooks() {

		if ( get_option( self::OPTION_NAME, 'no' ) === 'yes' ) {
			add_filter(
				'wc_product_sku_enabled',
				[ $this, 'is_product_sku_enabled' ],
				10,
				1
			);
		}
	}

	/**
	 * Returns `true` if the current product has a non-empty SKU, else `false`.
	 *
	 * @since [version]
	 * @param bool $enabled `true` if SKUs are enabled.
	 * @return bool
	 */
	public function is_product_sku_enabled( $enabled ) {

		/* @var WC_Product $product */
		global $product;

		if ( empty( $product ) ) {
			return $enabled;
		}

		$has_sku = ! empty( $product->get_sku() );

		if ( $has_sku === false && $product->get_type() === 'variable' ) {

			/* @var WC_Product_Variable $product */
			foreach ( $product->get_available_variations( 'objects' ) as $variation ) {

				/* @var WC_Product_Variation $variation */
				if ( ! empty( $variation->get_sku() ) ) {
					$has_sku = true;
					break;
				}
			}
		}

		return $has_sku;
	}
}
