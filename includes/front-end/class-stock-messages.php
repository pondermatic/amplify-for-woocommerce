<?php
/**
 * Front end Stock_Messages class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\PonderEventsForWooCommerce\Free\FrontEnd;

use Pondermatic\PonderEventsForWooCommerce\Free\Core;
use Pondermatic\PonderEventsForWooCommerce\Free\Models as Models;
use Pondermatic\PonderEventsForWooCommerce\Free\Traits\Hooks;
use WC_Product;

/**
 * Displays stock messages.
 *
 * @since 1.0.0
 * @todo feature: add do_action() and apply_filters()
 */
class Stock_Messages {

	use Hooks;

	/**
	 * The prefix of the WordPress option name used to hold stock messages data.
	 *
	 * @since 1.0.0
	 */
	public const OPTION_PREFIX = 'pefwc_stock_messages_';

	/**
	 * The prefix of the key used to hold posted stock messages data.
	 *
	 * @since 1.0.0
	 */
	public const POST_KEY_PREFIX = 'pefwc_stock_messages_';

	/**
	 * Used in the stock messages selector on a product edit page
	 * and in the database for wp_postmeta.meta_key.
	 *
	 * @since 1.0.0
	 */
	public const POST_META_KEY = '_pefwc_stock_messages_id';

	/**
	 * Add hook callbacks for use on public-facing pages.
	 *
	 * @since 1.0.0
	 */
	public function add_public_hooks() {

		add_filter(
			'woocommerce_get_availability_text',
			[ $this, 'filter_availability_text' ],
			10,
			2
		);
	}

	/**
	 * Get availability text based on stock status.
	 *
	 * @see WC_Product::get_availability_text()
	 * @since 1.0.0
	 * @param string     $availability Text.
	 * @param WC_Product $product      Product object.
	 * @return string
	 */
	public function filter_availability_text( $availability, $product ) {

		/* @var Models\Stock_Messages $stock_messages_model */
		$stock_messages_model = Core::get_namespace() . '\Models\Stock_Messages';
		$options              = $stock_messages_model::fetch_with_product( $product );
		if ( $options === false ) {
			$options = [];
		}

		if ( ! $product->is_in_stock() ) {
			if ( ! empty( $options['out_of_stock'] ) ) {
				$availability = $options['out_of_stock'];
			}
		} elseif ( $product->managing_stock() && $product->is_on_backorder( 1 ) ) {
			if ( $product->backorders_require_notification() ) {
				if ( ! empty( $options['available_on_backorder'] ) ) {
					$availability = $options['available_on_backorder'];
				}
			}
		} elseif ( ! $product->managing_stock() && $product->is_on_backorder( 1 ) ) {
			if ( ! empty( $options['available_on_backorder'] ) ) {
				$availability = $options['available_on_backorder'];
			}
		} elseif ( $product->managing_stock() ) {
			$availability = $this->format_stock_for_display( $product, $options );
		} else {
			$availability = '';
		}

		return $availability;
	}

	/**
	 * Format the "%s in stock" part of the WooCommerce availability text.
	 *
	 * Called when the WooCommerce 'Stock display format' option is set to
	 * 'Always show quantity remaining in stock e.g. "12 in stock"'.
	 *
	 * @see wc_format_stock_for_display()
	 * @since 1.0.0
	 * @param WC_Product $product Product object for which the stock you need to format.
	 * @param array      $options Settings.
	 * @param int        $stock_amount The quantity in stock.
	 * @return string
	 */
	protected function format_in_stock_for_display( $product, $options, $stock_amount ) {

		if ( empty( $options['in_stock_plural'] ) ) {
			/* translators: %s: stock amount */
			$format = __( '%s in stock', 'woocommerce' );
		} else {
			$format = $options['in_stock_plural'];
		}

		/* @noinspection PhpUnnecessaryLocalVariableInspection */
		$display = sprintf(
			$format,
			wc_format_stock_quantity_for_display( $stock_amount, $product )
		);

		return $display;
	}

	/**
	 * Format the "Only %s left in stock" part of the WooCommerce availability text.
	 *
	 * Called when the WooCommerce 'Stock display format' option is set to
	 * 'Only show quantity remaining in stock when low e.g. "Only 2 left in stock"'.
	 *
	 * @see wc_format_stock_for_display()
	 * @since 1.0.0
	 * @param WC_Product $product      Product object for which the stock you need to format.
	 * @param array      $options      Settings.
	 * @param int        $stock_amount The quantity in stock.
	 * @return string
	 */
	protected function format_low_stock_for_display( $product, $options, $stock_amount ) {

		if ( empty( $options['low_stock_plural'] ) ) {
			/* translators: %s: stock amount */
			$format = __( 'Only %s left in stock', 'woocommerce' );
		} else {
			$format = $options['low_stock_plural'];
		}

		/* @noinspection PhpUnnecessaryLocalVariableInspection */
		$display = sprintf(
			$format,
			wc_format_stock_quantity_for_display( $stock_amount, $product )
		);

		return $display;
	}

	/**
	 * Format the stock amount ready for display based on settings.
	 *
	 * @see wc_format_stock_for_display()
	 * @since 1.0.0
	 * @param WC_Product $product Product object for which the stock you need to format.
	 * @param array      $options Settings.
	 * @return string
	 */
	protected function format_stock_for_display( $product, $options ) {

		// Use this default unless the quantity is always shown or is above the low stock threshold.
		if ( empty( $options['in_stock'] ) ) {
			$display = __( 'In stock', 'woocommerce' );
		} else {
			$display = $options['in_stock'];
		}
		$stock_amount = $product->get_stock_quantity();

		switch ( get_option( 'woocommerce_stock_format' ) ) {

			// Always show quantity remaining in stock e.g. "12 in stock".
			case '':
				$display = $this->format_in_stock_for_display( $product, $options, $stock_amount );
				break;

			// Only show quantity remaining in stock when low e.g. "Only 2 left in stock".
			case 'low_amount':
				if ( $stock_amount <= wc_get_low_stock_amount( $product ) ) {
					$display = $this->format_low_stock_for_display( $product, $options, $stock_amount );
				}
				break;

			// Never show quantity remaining in stock.
			case 'no_amount':
				break;
		}

		if ( $product->backorders_allowed() && $product->backorders_require_notification() ) {

			if ( empty( $options['can_be_backordered'] ) ) {
				$display .= ' ' . __( '(can be backordered)', 'woocommerce' );
			} else {
				$display .= ' ' . $options['can_be_backordered'];
			}
		}

		return $display;
	}
}
